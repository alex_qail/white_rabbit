ymaps.ready(init);

$(function () {
    let typing = false;
    let gi = null;
    $('#address').on('change', function () {
        clearTimeout(gi);
        gi = setTimeout(()=>{
            console.log(this.value);
            customGeoCode(this.value);
        }, 2000);
    });
});
var myMap;
function init() {
    // Поиск координат центра Нижнего Новгорода.
    myMap = new ymaps.Map('map', {
        center: [55.753994, 37.622093],
        zoom: 9
    });
}

function addPlaceMark(geoObject = {}) {
    console.log('geoObject', geoObject);
    myMap.geoObjects.removeAll();
    let bounds = geoObject.properties.get('boundedBy');
    myMap.geoObjects.add(geoObject);
    myMap.setBounds(bounds, {
        // Проверяем наличие тайлов на данном масштабе.
        checkZoomRange: true
    });
}

function createDropdownMenu(listItems) {
    let list = document.createElement('ul');
    list.classList.add('dropdown');
    let dropdown = document.getElementById('select');
    for (let listItem of listItems)
    {
        let newItem = document.createElement('li');
        let coords = listItem.geometry.getCoordinates();
        newItem.dataset.latitude = coords[0];
        newItem.dataset.longitude = coords[1];
        newItem.innerText = listItem.getAddressLine();
        newItem.addEventListener('click', function(e, ...other) {
            addPlaceMark(listItem);
        });
        list.appendChild(newItem);
    }
    dropdown.appendChild(list);
}

function customGeoCode(strAddress = '') {
    console.log('myMap', myMap);
    // var myMap = new ymaps.Map('map', {
    //     center: [55.753994, 37.622093],
    //     zoom: 9
    // });
    ymaps.geocode(strAddress, {
        /**
         * Опции запроса
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
         */
        // Сортировка результатов от центра окна карты.
        //boundedBy: myMap.getBounds(),
        // strictBounds: true,
        // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
        // Если нужен только один результат, экономим трафик пользователей.
        //results: 5
    }).then(function (res) {
        // Выбираем первый результат геокодирования.
        var firstGeoObject = res.geoObjects.get(0),
            // Координаты геообъекта.
            coords = firstGeoObject.geometry.getCoordinates(),
            // Область видимости геообъекта.
            bounds = firstGeoObject.properties.get('boundedBy');
        let listItems = [];

        // cycle
        res.geoObjects.each(function (item) {

        firstGeoObject = item;
        coords = firstGeoObject.geometry.getCoordinates();
        firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
        // Получаем строку с адресом и выводим в иконке геообъекта.
        firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());

        //myMap.geoObjects.removeAll();
        // Добавляем первый найденный геообъект на карту.
        // myMap.geoObjects.add(firstGeoObject);
        // // Масштабируем карту на область видимости геообъекта.
        // myMap.setBounds(bounds, {
        //     // Проверяем наличие тайлов на данном масштабе.
        //     checkZoomRange: true
        // });

        console.log('coords', coords);
        /**
         * Все данные в виде javascript-объекта.
         */
        console.log('Все данные геообъекта: ', firstGeoObject.properties.getAll());
        /**
         * Метаданные запроса и ответа геокодера.
         * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderResponseMetaData.xml
         */
        console.log('Метаданные ответа геокодера: ', res.metaData);
        /**
         * Метаданные геокодера, возвращаемые для найденного объекта.
         * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderMetaData.xml
         */
        console.log('Метаданные геокодера: ', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData'));
        /**
         * Точность ответа (precision) возвращается только для домов.
         * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/precision.xml
         */
        console.log('precision', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.precision'));
        /**
         * Тип найденного объекта (kind).
         * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/kind.xml
         */
        console.log('Тип геообъекта: %s', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind'));
        console.log('Название объекта: %s', firstGeoObject.properties.get('name'));
        console.log('Описание объекта: %s', firstGeoObject.properties.get('description'));
        console.log('Полное описание объекта: %s', firstGeoObject.properties.get('text'));
        /**
         * Прямые методы для работы с результатами геокодирования.
         * @see https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/GeocodeResult-docpage/#getAddressLine
         */
        console.log('\nГосударство: %s', firstGeoObject.getCountry());
        console.log('Населенный пункт: %s', firstGeoObject.getLocalities().join(', '));
        console.log('Адрес объекта: %s', firstGeoObject.getAddressLine());
        console.log('Наименование здания: %s', firstGeoObject.getPremise() || '-');
        console.log('Номер здания: %s', firstGeoObject.getPremiseNumber() || '-');
       
        listItems.push(firstGeoObject);
        /**
         * Если нужно добавить по найденным геокодером координатам метку со своими стилями и контентом балуна, создаем новую метку по координатам найденной и добавляем ее на карту вместо найденной.
         */
        /**
         var myPlacemark = new ymaps.Placemark(coords, {
             iconContent: 'моя метка',
             balloonContent: 'Содержимое балуна <strong>моей метки</strong>'
             }, {
             preset: 'islands#violetStretchyIcon'
             });

         myMap.geoObjects.add(myPlacemark);
         */
        });
        createDropdownMenu(listItems);
    });
}