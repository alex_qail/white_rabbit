<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="price__list-wrapper">
    <?foreach($arResult["ITEMS"] as $arElement):?>
        <?
        $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div id="<?=$this->GetEditAreaId($arElement['ID']);?>" class="row price__item">
            <div class="col-xs-12 col-sm-10">
                <?php if(!empty($arElement["PROPERTIES"]["SERIES"]["VALUE"])):?>
                    <div class="price__item-series">
                        <?= $arElement["PROPERTIES"]["SERIES"]["VALUE"]; ?>
                    </div>
                <?php endif; ?>
                <div  class="price__item-title">
                    <a class="price__item-link" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a>
                </div>
                <?if(count($arElement["SECTION"]["PATH"])>0):?>
                    <br />
                    <?foreach($arElement["SECTION"]["PATH"] as $arPath):?>
                        / <a href="<?=$arPath["SECTION_PAGE_URL"]?>"><?=$arPath["NAME"]?></a>
                    <?endforeach?>
                <?endif?>
                <?foreach($arElement["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                    <div>
                        <?if(is_array($arProperty["DISPLAY_VALUE"]))
                            echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
                        elseif($arProperty["DISPLAY_VALUE"] === false)
                            echo "&nbsp;";
                        else
                            echo $arProperty["DISPLAY_VALUE"];?>
                    </div>
                <?endforeach?>
            </div>
            <?if(count($arResult["PRICES"]) > 0):?>
                <div class="col-xs-12 col-sm-2 price__value-wrapper">
                    <span class="price__value">
                        <?= $arElement["MIN_PRICE"]["PRINT_DISCOUNT_VALUE"] ?>
                    </span>
                </div>
            <?endif;?>
        </div>
    <?endforeach;?>
</div>
