<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;

class CustomHandler extends Base
{
    public static function getClassTitle()
    {
        return 'Доставка курьером по Новосибирску';
    }

    public static function getClassDescription()
    {
        return 'Доставка, стоимость которой зависит от района и суммы заказа';
    }

    protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment)
    {

        $result = new CalculationResult();
        $order = $shipment->getCollection()->getOrder();
        $orderPrice = $order->getBasket()->getPrice();
        if(!empty($_REQUEST["order"][DISTRICT_DELIVERY_INPUT])) {
            $customWidjets = [
                DISTRICT_DELIVERY_INPUT => [
                    'selected' => $_REQUEST["order"][DISTRICT_DELIVERY_INPUT],
                ]
            ];
            $currentDistrict = getSelectedDistrict($_REQUEST["order"][DISTRICT_DELIVERY_INPUT]);
        } else {
            $nskDistricts = getNskDistricts();
            $currentDistrict = array_shift($nskDistricts);
        }

            if(!empty($currentDistrict["PROPS"]["ADD_CONDITION_SUMM"]["VALUE"]) && $orderPrice >= $currentDistrict["PROPS"]["ADD_CONDITION_SUMM"]["VALUE"] && !empty($currentDistrict["PROPS"]["ADD_DELIVERY_PRICE"]["VALUE"])) {
                $result->setDeliveryPrice(roundEx($currentDistrict["PROPS"]["ADD_DELIVERY_PRICE"]["VALUE"], 2));
            } elseif(!empty($currentDistrict["PROPS"]["MIN_ORDER_FROM"]["VALUE"]) && !empty($currentDistrict["PROPS"]["MIN_ORDER_TO"]["VALUE"]) && $orderPrice <= $currentDistrict["PROPS"]["MIN_ORDER_TO"]["VALUE"] && $orderPrice > $currentDistrict["PROPS"]["MIN_ORDER_FROM"]["VALUE"] && !empty($currentDistrict["PROPS"]["DELIVERY_PRICE"]["VALUE"])) {
                $result->setDeliveryPrice(roundEx($currentDistrict["PROPS"]["DELIVERY_PRICE"]["VALUE"], 2));
            } elseif (!empty($currentDistrict["PROPS"]["MIN_ORDER_FROM"]["VALUE"]) && $orderPrice > $currentDistrict["PROPS"]["MIN_ORDER_FROM"]["VALUE"] && !empty($currentDistrict["PROPS"]["DELIVERY_PRICE"]["VALUE"])) {
                $result->setDeliveryPrice(roundEx($currentDistrict["PROPS"]["DELIVERY_PRICE"]["VALUE"], 2));
            }

            $curDayOfWeek = strtoupper(date('N'));
            $currentDistrict["PROPS"]["DELIVERY_TIME"]["VALUE_XML_ID"];
            if($currentDistrict["PROPS"]["DELIVERY_TIME"]["VALUE_XML_ID"] > $curDayOfWeek) {
                $deliveryDays = $currentDistrict["PROPS"]["DELIVERY_TIME"]["VALUE_XML_ID"] - $curDayOfWeek;
            } else {
                $deliveryDays = 7;
            }
            $deliveryDate = date('d.m.Y', strtotime("+${$deliveryDays} days"));

            $result->setPeriodDescription("<span>${$deliveryDate}</span>");

            $result->setTmpData($customWidjets);

        return $result;
    }

    protected function getConfigStructure()
    {
        return array(
            "MAIN" => array(
                "TITLE" => 'Настройка обработчика',
                "DESCRIPTION" => 'Настройка обработчика',"ITEMS" => array(
                    "PRICE" => array(
                        "TYPE" => "NUMBER",
                        "MIN" => 0,
                        "NAME" => 'Стоимость доставки за грамм'
                    )
                )
            )
        );
    }

    public function isCalculatePriceImmediately()
    {
        return true;
    }

    public static function whetherAdminExtraServicesShow()
    {
        return true;
    }
}
?>