<?php
//AddEventHandler("sale", "OnOrderUpdate", "OnOrderUpdateHandler");
//
//function OnOrderUpdateHandler(&$ID, &$arFields)
//{
//    if (CModule::IncludeModule('sale')) {
//        $dbItemsInOrder = CSaleBasket::GetList(array("ID" => "ASC"), array("ORDER_ID" => intval($ID)));
//
//        while ($arItems = $dbItemsInOrder->Fetch()) {
//            //...
//        }
//    }
//}

define("FRANCHISE_IBLOCK_ID", 38);
define("DISTRICTS_IBLOCK_ID", 42);
define("DISTRICTS_DELIVERY_ID", 64);
define("PICKUP_DELIVERY_ID", 3);
define("INFO_IBLOCK_ID", 41);
define("DOCUMENTS_IBLOCK_ID", 40);
define("MAIL_TO_FRANCHISE_MEMBER_TEMPLATE_ID", 63);
define("STORES_IBLOCK_ID", 14);
define("CURRENT_CITY_CODE", "0000625079");
define("DISTRICT_DELIVERY_INPUT", "district_id");


AddEventHandler("sale", "OnOrderSave", "OnOrderSaveHandler");

AddEventHandler("sale", "OnSaleComponentOrderProperties", "fillOrderLocation");

function getDeliveryDay()
{
    $curDayOfWeek = strtoupper(date('l'));

}

function getStores()
{
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID"=>STORES_IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $queryRes = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    $result = [];
    while($ob = $queryRes->GetNextElement()){
        $newItem = [];
        $arFields = $ob->GetFields();
        $newItem['FIELDS'] = $arFields;
        $arProps = $ob->GetProperties();
        $newItem['PROPS'] = $arProps;
        $result[]=$newItem;
    }
    return $result;
}

function getStoreIds()
{
    $stores = getStores();
    $ids = [];
    foreach ($stores as $store) {
        $ids[]= $store['FIELDS']["ID"];
    }
    return $ids;
}

function getNskDistricts()
{
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID"=>DISTRICTS_IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $queryRes = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    $result = [];
    while($ob = $queryRes->GetNextElement()){
        $newItem = [];
        $arFields = $ob->GetFields();
        $newItem['FIELDS'] = $arFields;
        $arProps = $ob->GetProperties();
        $newItem['PROPS'] = $arProps;
        $result[]=$newItem;
    }
    return $result;
}

function filterNskDistrictsByOrderSum($orderSum)
{
    $nskDistricts = getNskDistricts();
    $result = [];
    foreach ($nskDistricts as $nskDistrict) {
        if((!empty($nskDistrict["PROPS"]["MIN_ORDER_FROM"]["VALUE"]) && $orderSum >= $nskDistrict["PROPS"]["MIN_ORDER_FROM"]["VALUE"]) || empty($nskDistrict["PROPS"]["MIN_ORDER_FROM"]["VALUE"])) {
            $result[]= $nskDistrict;
        }
    }
    return $result;
}

function getSelectedDistrict($id)
{
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID"=>DISTRICTS_IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID" => $id);
    $queryRes = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    $result = [];
    if($ob = $queryRes->GetNextElement()){
        $arFields = $ob->GetFields();
        $result['FIELDS'] = $arFields;
        $arProps = $ob->GetProperties();
        $result['PROPS'] = $arProps;
    }
    return $result;
}


function showSectionGalleryCustom( $params = array() ){
    $arItem = isset($params['ITEM']) ? $params['ITEM'] : array();
    $key = isset($params['GALLERY_KEY']) ? $params['GALLERY_KEY'] : 'GALLERY';
    $bReturn = isset($params['RETURN']) ? $params['RETURN'] : false;
    $arResize = isset($params['RESIZE']) ? $params['RESIZE'] : array('WIDTH' => 600, 'HEIGHT' => 600);

    if($arItem):?>
        <?ob_start();?>
        <?if($arItem[$key]):?>
            <?$count = count($arItem[$key]);?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="thumb<?=($bReturn ? '' : ($count > 1 ? '' : ' shine'));?>">
							<span class="section-gallery-wrapper flexbox">
								<?foreach($arItem[$key] as $i => $arGalleryItem):?>
                                    <?
                                    if($arResize) {
                                        $resizeImage = \CFile::ResizeImageGet($arGalleryItem["ID"], array("width" => $arResize['WIDTH'], "height" => $arResize['HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true, array());
                                        $arGalleryItem['SRC'] = $resizeImage['src'];
                                        $arGalleryItem['HEIGHT'] = $resizeImage['height'];
                                        $arGalleryItem['WIDTH'] = $resizeImage['width'];
                                    }?>
                                    <span class="section-gallery-wrapper__item<?=( $i ? ' _active' : '');?>">
										<span class="section-gallery-wrapper__item-nav<?=($count > 1 ? ' ' : ' section-gallery-wrapper__item_hidden ');?>"></span>
										<img class="lazy img-responsive" src="<?=\Aspro\Functions\CAsproMax::showBlankImg($arGalleryItem["SRC"]);?>" data-src="<?=$arGalleryItem["SRC"]?>" alt="<?=$arGalleryItem["ALT"];?>" title="<?=$arGalleryItem["TITLE"];?>" />
									</span>
                                <?endforeach;?>
							</span>
            </a>
        <?else:?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="thumb"><img class="lazy img-responsive " data-src="<?=SITE_TEMPLATE_PATH?>/images/svg/noimage_product.svg" src="<?=\Aspro\Functions\CAsproMax::showBlankImg(SITE_TEMPLATE_PATH.'/images/svg/noimage_product.svg');?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
        <?endif;?>
        <?$html = ob_get_contents();
        ob_end_clean();

        foreach(GetModuleEvents(FUNCTION_MODULE_ID, 'OnAsproShowSectionGallery', true) as $arEvent) // event for manipulation item img
            ExecuteModuleEventEx($arEvent, array($arItem, &$html));

        if(!$bReturn)
            echo $html;
        else
            return $html;
    endif;
}

function getOfferAdditionalPrices($offers = [], $offerIndex = 0, $pricePrefix = "PRICE_")
{
    $result = [];
    foreach ($offers[$offerIndex]["DISPLAY_PROPERTIES"] as $prop)
    {
        if(strpos($prop["CODE"], $pricePrefix) === 0)
        {
            $result[]= $prop;
        }
    }
    return $result;
}

function fillOrderLocation(&$arUserResult, $request, &$arParams, &$arResult)
{
    $registry = \Bitrix\Sale\Registry::getInstance(\Bitrix\Sale\Registry::REGISTRY_TYPE_ORDER);
    $orderClassName = $registry->getOrderClassName();
    $order = $orderClassName::create(\Bitrix\Main\Application::getInstance()->getContext()->getSite());
    $propertyCollection = $order->getPropertyCollection();
    foreach ($propertyCollection as $property)
    {
        if ($property->isUtil())
            continue;
        $arProperty = $property->getProperty();

        if(
            $arProperty['TYPE'] === 'LOCATION'
            && array_key_exists($arProperty['ID'],$arUserResult["ORDER_PROP"])
            && !$request->getPost("ORDER_PROP_".$arProperty['ID'])
            && (
                !is_array($arOrder=$request->getPost("order"))
                || !$arOrder["ORDER_PROP_".$arProperty['ID']]
            )
        ) {
            $arUserResult["ORDER_PROP"][$arProperty['ID']] = CURRENT_CITY_CODE;
            file_put_contents(__DIR__."/l.txt", print_r($arUserResult["ORDER_PROP"][$arProperty['ID']], true));
        }
        //$arUserResult["ORDER_PROP"][$arProperty['ID']] = CURRENT_CITY_CODE;
    }
}


function calcDistanceByCoords($arCoords1, $arCoords2)
{
    $lat1 = $arCoords1[0];
    $long1 = $arCoords1[1];
    $lat2 = $arCoords2[0];
    $long2 = $arCoords2[1];
    $deltaLat = abs($lat1 - $lat2);
    $deltaLong = abs($long1 - $long2);
    $distance = sqrt(pow($deltaLat, 2) + pow($deltaLong, 2));
    return $distance;
}

function getFranchiseStores($iblockId = FRANCHISE_IBLOCK_ID)
{
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_LATITUDE", "PROPERTY_LONGITUDE", "PROPERTY_EMAIL");
    $arFilter = Array("IBLOCK_ID"=>$iblockId, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    $franchizeStores = [];
    while($ob = $res->GetNextElement())
    {
        $store = [];
        $arFields = $ob->GetFields();
        $store["fields"] = $arFields;
        $arProps = $ob->GetProperties();
        $store["props"] = $arProps;
        $franchizeStores[]=$store;
    }
    return $franchizeStores;
}

function OnOrderSaveHandler(&$ID, &$arFields, &$arOrder, $isNew)
{
    if($isNew) {
        $dbOrderProps = CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array("ORDER_ID" => $ID, "CODE"=>array("LATITUDE", "LONGITUDE"))
        );
        $orderProps = [];
        while ($arOrderProps = $dbOrderProps->GetNext()){
            $orderProps[$arOrderProps['CODE']]=$arOrderProps['VALUE'];
        }
        $shipmentAddressCoords = [$orderProps["LATITUDE"], $orderProps["LONGITUDE"]];

        $franchizeStores = getFranchiseStores();
        $shortestDistance = null;
        $id = null;
        $storeEmail = null;
        foreach ($franchizeStores as $franchizeStore)
        {
            $storeCoords = [];
            $storeCoords []= $franchizeStore["props"]["LATITUDE"]["VALUE"];
            $storeCoords []= $franchizeStore["props"]["LONGITUDE"]["VALUE"];
            $distance = calcDistanceByCoords($shipmentAddressCoords, $storeCoords);
            if($distance < $shortestDistance || !$shortestDistance)
            {
                $shortestDistance = $distance;
                $id = $franchizeStore["fields"]["ID"];
                $storeEmail = $franchizeStore["props"]["EMAIL"]["VALUE"];
            }
        }
        if($id && $storeEmail)
        {
            $order = \Bitrix\Sale\Order::load($arOrder["ID"]);
            if ($order)
            {
                $propertyCollection = $order->getPropertyCollection();
                $sipmentStoreeId = getPropertyByCode($propertyCollection, 'SHIPMENT_STORE');
                $sipmentStoreeId->setValue($id);
                $order->save();
            }
            //
            $mailFields = array(
                "EMAIL_TO"            => $storeEmail,
                "ADMIN_EMAIL"         => "sale@cs86123.tmweb.ru",
                "ORDER_ID" => $arOrder["ID"],
            );
            file_put_contents(__DIR__."/mailFields.txt", print_r($mailFields, true));

            if(CEvent::Send('SALE_NEW_ORDER', SITE_ID, $mailFields, 'N', MAIL_TO_FRANCHISE_MEMBER_TEMPLATE_ID))
            {
                file_put_contents(__DIR__."/email.txt", print_r('ok', true));
            }
            else{
                file_put_contents(__DIR__."/email.txt", print_r('error', true));
            }
        }
    }
}

function getPropertyByCode($propertyCollection, $code) {
    foreach ($propertyCollection as $property) {
        if($property->getField('CODE') == $code) {
            return $property;
        }
    }
}