BX.namespace('BX.Sale.OrderAjaxComponent.Maps');

(function() {
	'use strict';

	BX.Sale.OrderAjaxComponent.Maps = {
		addressInputTimeout: 1500,
		latitudeInput: document.getElementById('soa-property-20'),
		longitudeInput: document.getElementById('soa-property-21'),

		init: function(ctx)
		{
			this.context = ctx || {};
			this.pickUpOptions = this.context.options.pickUpMap;
			console.log('this.pickUpOptions', this.pickUpOptions );
			this.propsOptions = this.context.options.propertyMap;
			this.maxWaitTimeExpired = false;

			return this;
		},

		initializePickUpMap: function(selected)
		{
			if (!ymaps)
				return;

			this.pickUpMap = new ymaps.Map('pickUpMap', {
				center: !!selected
					? [selected.GPS_N, selected.GPS_S]
					: [this.pickUpOptions.defaultMapPosition.lat, this.pickUpOptions.defaultMapPosition.lon],
				zoom: this.pickUpOptions.defaultMapPosition.zoom
			});

			this.pickUpMap.behaviors.disable('scrollZoom');

			this.pickUpMap.events.add('click', BX.delegate(function(){
				if (this.pickUpMap.balloon.isOpen())
				{
					this.pickUpMap.balloon.close();
				}
			}, this));
		},

		pickUpMapFocusWaiter: function()
		{
			if (this.pickUpMap && this.pickUpMap.geoObjects)
			{
				this.setPickUpMapFocus();
			}
			else
			{
				setTimeout(BX.proxy(this.pickUpMapFocusWaiter, this), 100);
			}
		},

		setPickUpMapFocus: function()
		{
			var bounds, diff0, diff1;

			bounds = this.pickUpMap.geoObjects.getBounds();
			if (bounds && bounds.length)
			{
				diff0 = bounds[1][0] - bounds[0][0];
				diff1 = bounds[1][1] - bounds[0][1];

				bounds[0][0] -= diff0/10;
				bounds[0][1] -= diff1/10;
				bounds[1][0] += diff0/10;
				bounds[1][1] += diff1/10;

				this.pickUpMap.setBounds(bounds, {checkZoomRange: true});
			}
		},

		showNearestPickups: function(successCb, failCb)
		{
			if (!ymaps)
				return;

			var provider = this.pickUpOptions.secureGeoLocation && BX.browser.IsChrome() && !this.context.isHttps
				? 'yandex'
				: 'auto';
			var maxTime = this.pickUpOptions.geoLocationMaxTime || 5000;

			ymaps.geolocation.get({
				provider: provider,
				timeOut: maxTime
			}).then(
				BX.delegate(function(result){
					if (!this.maxWaitTimeExpired)
					{
						this.maxWaitTimeExpired = true;

						result.geoObjects.options.set('preset', 'islands#darkGreenCircleDotIcon');
						this.pickUpMap.geoObjects.add(result.geoObjects);

						successCb(result);
					}
				}, this),
				BX.delegate(function() {
					if (!this.maxWaitTimeExpired)
					{
						this.maxWaitTimeExpired = true;

						failCb();
					}
				}, this)
			);
		},

		buildBalloons: function(activeStores)
		{
			if (!ymaps)
				return;

			var that = this;

			this.pickUpPointsJSON = [];

			for (var i = 0; i < activeStores.length; i++)
			{
				var storeInfoHtml = this.getStoreInfoHtml(activeStores[i]);

				this.pickUpPointsJSON.push({
					type: 'Feature',
					geometry: {type: 'Point', coordinates: [activeStores[i].GPS_N, activeStores[i].GPS_S]},
					properties: {storeId: activeStores[i].ID}
				});

				var geoObj = new ymaps.Placemark([activeStores[i].GPS_N, activeStores[i].GPS_S], {
					hintContent: BX.util.htmlspecialchars(activeStores[i].TITLE) + '<br />' + BX.util.htmlspecialchars(activeStores[i].ADDRESS),
					storeTitle: activeStores[i].TITLE,
					storeBody: storeInfoHtml,
					id: activeStores[i].ID,
					text: this.context.params.MESS_SELECT_PICKUP
				}, {
					balloonContentLayout: ymaps.templateLayoutFactory.createClass(
						'<h3>{{ properties.storeTitle }}</h3>' +
						'{{ properties.storeBody|raw }}' +
						'<br /><a class="btn btn-sm btn-default" data-store="{{ properties.id }}">{{ properties.text }}</a>',
						{
							build: function() {
								this.constructor.superclass.build.call(this);

								var button = document.querySelector('a[data-store]');
								if (button)
									BX.bind(button, 'click', this.selectStoreByClick);
							},
							clear: function() {
								var button = document.querySelector('a[data-store]');
								if (button)
									BX.unbind(button, 'click', this.selectStoreByClick);

								this.constructor.superclass.clear.call(this);
							},
							selectStoreByClick: function(e) {
								var target = e.target || e.srcElement;

								if (that.pickUpMap.container.isFullscreen())
								{
									that.pickUpMap.container.exitFullscreen();
								}

								that.context.selectStore(target.getAttribute('data-store'));
								that.context.clickNextAction(e);
								that.pickUpMap.balloon.close();
							}
						}
					)
				});

				if (BX('BUYER_STORE').value === activeStores[i].ID)
				{
					geoObj.options.set('preset', 'islands#redDotIcon');
				}

				this.pickUpMap.geoObjects.add(geoObj);
			}
		},

		selectBalloon: function(storeItemId)
		{
			if (this.pickUpMap && this.pickUpMap.geoObjects)
			{
				this.pickUpMap.geoObjects.each(BX.delegate(function(placeMark){
					if (placeMark.properties.get('id'))
					{
						placeMark.options.unset('preset');
					}

					if (placeMark.properties.get('id') === storeItemId)
					{
						placeMark.options.set({preset: 'islands#redDotIcon'});
						this.pickUpMap.panTo([placeMark.geometry.getCoordinates()])
					}
				}, this));
			}
		},

		pickUpFinalAction: function()
		{
			if (this.pickUpMap && this.pickUpMap.geoObjects)
			{
				var buyerStoreInput = BX('BUYER_STORE');

				this.pickUpMap.geoObjects.each(function(geoObject){
					if (geoObject.properties.get('id') === buyerStoreInput.value)
					{
						geoObject.options.set({preset: 'islands#redDotIcon'});
					}
					else if (parseInt(geoObject.properties.get('id')) > 0)
					{
						geoObject.options.unset('preset');
					}
				});
			}
		},

		initializePropsMap: function(propsMapData)
		{
			if (!ymaps)
				return;

			this.propsMap = new ymaps.Map('propsMap', {
				center: [propsMapData.lat, propsMapData.lon],
				zoom: propsMapData.zoom
			});

			this.addressPrefixElement = document.getElementById('addressPrefix');
            this.addressPrefix = this.addressPrefixElement.innerText;
            console.log('this.addressPrefix 1', this.addressPrefix);

			this.propsMap.behaviors.disable('scrollZoom');

			this.propsMap.events.add('click', BX.delegate(function(e){
				var coordinates = e.get('coords'), placeMark;

				if (this.propsMap.geoObjects.getLength() === 0)
				{
					placeMark = new ymaps.Placemark([coordinates[0], coordinates[1]], {}, {
						draggable:true,
						preset: 'islands#redDotIcon'
					});
					placeMark.events.add(['parentchange', 'geometrychange'], function() {
						var orderDesc = BX('orderDescription'),
							coordinates = placeMark.geometry.getCoordinates(),
							ind, before, after, string;

						if (orderDesc)
						{
							ind = orderDesc.value.indexOf(BX.message('SOA_MAP_COORDS') + ':');
							if (ind === -1)
							{
								orderDesc.value = BX.message('SOA_MAP_COORDS') + ': ' + coordinates[0] + ', '
									+ coordinates[1] + '\r\n' + orderDesc.value;
							}
							else
							{
								string = BX.message('SOA_MAP_COORDS') + ': ' + coordinates[0] + ', ' + coordinates[1];
								before = orderDesc.value.substring(0, ind);
								after = orderDesc.value.substring(ind + string.length);
								orderDesc.value = before + string + after;
							}
						}
					});

					this.propsMap.geoObjects.add(placeMark);
				}
				else
				{
					this.propsMap.geoObjects.get(0).geometry.setCoordinates([coordinates[0], coordinates[1]]);
				}
			}, this));

			// custom_code
			let addressInput = BX('addressInput');
            let addressInputPrefixWrapper = BX('input-prefix-wrapper');
			console.log('addressInput', addressInput);
			let timeoutId;
			let context = this;
            addressInput.addEventListener('keyup', function(e) {
                clearTimeout(timeoutId);
                addressInputPrefixWrapper.classList.add('loading');
                timeoutId = setTimeout(() => {
                	console.log('this.value', this.value);
                    console.log(context.addressPrefix + ' ' + this.value);
                    context.geoCode(context.addressPrefix + ' ' + this.value);
                }, context.addressInputTimeout);
            });
            // /custom_code
		},

        addPlaceMark: function(geoObject = {}) {
            console.log('geoObject', geoObject);
            this.propsMap.geoObjects.removeAll();
            let bounds = geoObject.properties.get('boundedBy');
            this.propsMap.geoObjects.add(geoObject);
            this.propsMap.setBounds(bounds, {
                // Проверяем наличие тайлов на данном масштабе.
                checkZoomRange: true
            });
        },

        createDropdownMenu: function(listItems) {
			let existedList = document.getElementById('address-geocode-dropdown');
            let list = existedList ? existedList : document.createElement('ul');
            if(!existedList)
			{
                list.id = 'address-geocode-dropdown';
                list.classList.add('address-geocode-dropdown');
			}
			else
			{
                list.innerHTML = null;
			}
            let addressInput = document.getElementById('addressInput');
            let defaultAddressInput = document.getElementById('soa-property-7');
            let addressPrefix = document.getElementById('addressPrefix');
            let prefix = addressPrefix.innerText;
            let addressInputWrapper = document.getElementById('addressInputWrapper');
            let addressPrefixWrapper = document.getElementById('input-prefix-wrapper');
            let latInput = document.getElementById('soa-property-20');
            let longInput = document.getElementById('soa-property-21');
            console.log('latInput', latInput);
            for (let listItem of listItems)
            {
                let newItem = document.createElement('li');
                let coords = listItem.geometry.getCoordinates();
                newItem.dataset.latitude = coords[0];
                newItem.dataset.longitude = coords[1];
                newItem.innerText = listItem.getAddressLine();
                let context = this;
                newItem.addEventListener('click', function(e) {
                    addressPrefixWrapper.classList.remove('loading');
                    context.addPlaceMark(listItem);
                    if (this.innerText.indexOf(prefix) === 0)
					{
                        addressInput.value = this.innerText.substr(prefix.length);
					}
					else
					{
                        addressInput.value = this.innerText;
                        addressPrefix.innerText = "";
                        addressPrefix.classList.add("empty");
					}
                    //addressInput.value = this.innerText.substr(prefix.length);
                    defaultAddressInput.value = this.innerText;
                    latInput.value = coords[0];
                    longInput.value = coords[1];
                    list.remove();
                });
                list.appendChild(newItem);
            }
            addressInputWrapper.appendChild(list);
        },

        geoCode: function(strAddress = '')
		{
			let context = this;
            ymaps.geocode(strAddress, {
                /**
                 * Опции запроса
                 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                 */
                // Сортировка результатов от центра окна карты.
                //boundedBy: myMap.getBounds(),
                // strictBounds: true,
                // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
                // Если нужен только один результат, экономим трафик пользователей.
                //results: 5
            }).then(function (res) {
                // Выбираем первый результат геокодирования.
                var firstGeoObject = res.geoObjects.get(0),
                    // Координаты геообъекта.
                    coords = firstGeoObject.geometry.getCoordinates(),
                    // Область видимости геообъекта.
                    bounds = firstGeoObject.properties.get('boundedBy');
                let listItems = [];

                // cycle
                res.geoObjects.each(function (item) {

                    firstGeoObject = item;
                    coords = firstGeoObject.geometry.getCoordinates();
                    firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
                    // Получаем строку с адресом и выводим в иконке геообъекта.
                    firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());

                    //myMap.geoObjects.removeAll();
                    // Добавляем первый найденный геообъект на карту.
                    // myMap.geoObjects.add(firstGeoObject);
                    // // Масштабируем карту на область видимости геообъекта.
                    // myMap.setBounds(bounds, {
                    //     // Проверяем наличие тайлов на данном масштабе.
                    //     checkZoomRange: true
                    // });

                    console.log('coords', coords);
                    /**
                     * Все данные в виде javascript-объекта.
                     */
                    console.log('Все данные геообъекта: ', firstGeoObject.properties.getAll());
                    /**
                     * Метаданные запроса и ответа геокодера.
                     * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderResponseMetaData.xml
                     */
                    console.log('Метаданные ответа геокодера: ', res.metaData);
                    /**
                     * Метаданные геокодера, возвращаемые для найденного объекта.
                     * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderMetaData.xml
                     */
                    console.log('Метаданные геокодера: ', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData'));
                    /**
                     * Точность ответа (precision) возвращается только для домов.
                     * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/precision.xml
                     */
                    console.log('precision', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.precision'));
                    /**
                     * Тип найденного объекта (kind).
                     * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/kind.xml
                     */
                    console.log('Тип геообъекта: %s', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind'));
                    console.log('Название объекта: %s', firstGeoObject.properties.get('name'));
                    console.log('Описание объекта: %s', firstGeoObject.properties.get('description'));
                    console.log('Полное описание объекта: %s', firstGeoObject.properties.get('text'));
                    /**
                     * Прямые методы для работы с результатами геокодирования.
                     * @see https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/GeocodeResult-docpage/#getAddressLine
                     */
                    console.log('\nГосударство: %s', firstGeoObject.getCountry());
                    console.log('Населенный пункт: %s', firstGeoObject.getLocalities().join(', '));
                    console.log('Адрес объекта: %s', firstGeoObject.getAddressLine());
                    console.log('Наименование здания: %s', firstGeoObject.getPremise() || '-');
                    console.log('Номер здания: %s', firstGeoObject.getPremiseNumber() || '-');

                    listItems.push(firstGeoObject);
                    /**
                     * Если нужно добавить по найденным геокодером координатам метку со своими стилями и контентом балуна, создаем новую метку по координатам найденной и добавляем ее на карту вместо найденной.
                     */
                    /**
                     var myPlacemark = new ymaps.Placemark(coords, {
             iconContent: 'моя метка',
             balloonContent: 'Содержимое балуна <strong>моей метки</strong>'
             }, {
             preset: 'islands#violetStretchyIcon'
             });

                     myMap.geoObjects.add(myPlacemark);
                     */
                });
                context.createDropdownMenu(listItems);
            });
        },

		canUseRecommendList: function()
		{
			return (this.pickUpPointsJSON && this.pickUpPointsJSON.length);
		},

		getRecommendedStoreIds: function(geoLocation)
		{
			if (!geoLocation)
				return [];

			var storeIds = [];
			var length = this.pickUpPointsJSON.length < this.pickUpOptions.nearestPickUpsToShow
					? this.pickUpPointsJSON.length
					: this.pickUpOptions.nearestPickUpsToShow;

			this.storeQueryResult = {};

			for (var i = 0; i < length; i++)
			{
				var pointsGeoQuery = ymaps.geoQuery({
					type: 'FeatureCollection',
					features: this.pickUpPointsJSON
				});
				var res = pointsGeoQuery.getClosestTo(geoLocation);
				var storeId = res.properties.get('storeId');

				this.storeQueryResult[storeId] = res;
				storeIds.push(storeId);
				this.pickUpPointsJSON.splice(pointsGeoQuery.indexOf(res), 1);
			}

			return storeIds;
		},

		getDistance: function(geoLocation, storeId)
		{
			if (!geoLocation || !storeId)
				return false;

			var storeGeoQuery = this.storeQueryResult[storeId];
			var distance = ymaps.coordSystem.geo.getDistance(geoLocation.geometry.getCoordinates(), storeGeoQuery.geometry.getCoordinates());
			distance = Math.round(distance / 100) / 10;

			return distance;
		},

		propsMapFocusWaiter: function(){},

		getStoreInfoHtml: function(currentStore)
		{
			var html = '';

			if (currentStore.ADDRESS)
				html += BX.message('SOA_PICKUP_ADDRESS') + ': ' + BX.util.htmlspecialchars(currentStore.ADDRESS) + '<br />';

			if (currentStore.PHONE)
				html += BX.message('SOA_PICKUP_PHONE') + ': ' + BX.util.htmlspecialchars(currentStore.PHONE) + '<br />';

			if (currentStore.SCHEDULE)
				html += BX.message('SOA_PICKUP_WORK') + ': ' + BX.util.htmlspecialchars(currentStore.SCHEDULE) + '<br />';

			if (currentStore.DESCRIPTION)
				html += BX.message('SOA_PICKUP_DESC') + ': ' + BX.util.htmlspecialchars(currentStore.DESCRIPTION) + '<br />';

			return html;
		}
	};
})();